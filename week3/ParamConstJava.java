import java.util.Scanner;
class ParamConstJava
{
    public void student()
    {
        String collegename = "MVGR";
        int collegecode = 33;
        System.out.println("CollegeName : "+collegename);
        System.out.println("CollegeCode : "+collegecode);
    }
    public void student(String name , double sempercentage)
    {
        System.out.println("Student Name : "+name);
        System.out.println("Student SemPercentage : "+sempercentage);
    }
  
    public static void main (String[] args) {
        String name;
        double percentage;
        Scanner input = new Scanner(System.in);
        System.out.println("Enter your name : ");
        name=input.nextLine();
        System.out.println("Enter SemPercentage : ");
        percentage=input.nextDouble();
        ParamConstJava obj = new ParamConstJava();
        obj.student(name,percentage);
        obj.student();
        
    }
}