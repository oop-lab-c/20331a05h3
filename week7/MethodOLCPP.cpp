#include<iostream>
using namespace std;

class Myclass
{
    public:
        void display(int n)
        {
            cout<<"The value is : "<<n<<endl;
        }
        void display(string name)
        {
            cout<<"The name is : "<<name<<endl;
        }
};

int main()
{
    Myclass obj;
    obj.display(457);
    obj.display("Bhargav Ram");
}

