class parent
{
    void fun(int a, int b)
    {
        System.out.println(a*b+"\n");

    }
}
class MethodOlInheriJava extends parent
{
    void fun(int a, int b, int c)
    {
        System.out.println(a*b*c);
    }
    public static void main(String[] args)
    {
        MethodOlInheriJava obj = new MethodOlInheriJava();
        obj.fun(45,57);
        obj.fun(4,5,7);
    }
    
}
