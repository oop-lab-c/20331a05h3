class parent
{
    void display()
    {
        System.out.println("I am parent !");
    }

}
class MethodOrJava extends parent
{
    void display()
    {
        System.out.println("I am child !");
    }
    public static void main(String[] args)
    {
        MethodOrJava obj = new MethodOrJava();
        obj.display();
    }
    
}