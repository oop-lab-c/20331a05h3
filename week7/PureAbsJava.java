interface Parent{
   void display();
}
class Child1 implements Parent{
   public void display() {
      System.out.println("This is display method of the Child1 class\n");
   }
}
class Child2 implements Parent{
   public void display() {
      System.out.println("This is display method of the Child2 class");
   }
}
class PureAbsJava{
   public static void main(String args[]) {
      Parent obj1 = new Child1();
      obj1.display();
      Parent obj2 = new Child2();
      obj2.display();
   }
}