import java.util.*;

abstract class Animal 
{
    public abstract void animalSound();
    public void sleep() 
    {
      System.out.println("Zzz");
    }
  }
  
  
  class cat extends Animal 
  {
    public void animalSound() 
    {
      System.out.println("The cats says: meow meow");
    }
  }
  
  class ParAbsJava {
    public static void main(String[] args) {
      cat obj  = new cat(); 
      obj.animalSound();
      obj.sleep();
    }
  }
  