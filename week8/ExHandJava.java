import java.util.*;
class ExHandJava
{
    public static void main(String[] args) 
    {
            Scanner input = new Scanner(System.in);
            System.out.println("Enter the Numerator value :");
            int a = input.nextInt();
            System.out.println("Enter the Denominator value :");
            int b = input.nextInt();
            int c;
            try
            {
                c=a/b;
            }
            catch(ArithmeticException e)
            {
                System.out.println("Denominator cannot be zero");
            }
    }
}