import java.util.*;
class userex extends Exception
{
    userex()
    {
        super("Denominator should not be 0");
    }
}
class UserExHandJava
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the numerator :");
        int n = input.nextInt();
        System.out.println("Enter the denominator :");
        int d = input.nextInt();
        try
        {
            if(d==0)
            {
                throw new userex();
            }
        }
        catch(userex e)
        {
            System.out.println(e.toString());
            e.printStackTrace();
        }
        finally
        {
            System.out.println("Process is completed.");
        }

    }
}
