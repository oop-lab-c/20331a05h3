#include<iostream>
using namespace std;
class AbsEncapCPP
{
    private:
    int priVar;
    protected:
    int proVar;
    public:
    int pubVar;
    void setVar(int priValue,int proValue,int pubValue)
    {
        priVar=priValue;
        proVar=proValue;
        pubVar=pubValue;
    }
    void getVar()
    {
        cout<<"Private Variable : "<<priVar<<endl;
        cout<<"Protected Variable : "<<proVar<<endl;
        cout<<"Public Variable : "<<pubVar<<endl;
    }
};
int main()
{
    AbsEncapCPP obj;
    obj.setVar(45,57,74);
    obj.getVar();
    return 0;
}