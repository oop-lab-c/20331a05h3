#include<iostream>
using namespace std;
class Inh
{
  public:
  int a=45;
  protected:
  int b=57;
  private:
  int c=74;
  public:
  int pri() 
  {
      return c;
  }
};
class publicInh : public Inh
{
  public:
  int pro()
  {
      return b;
  }
};
class protectedInh : protected Inh {
  public:
    int pro() {
      return b;
    }
    int pub() {
      return a;
    }
};
class privateInh : private Inh {
  public:
    int pro() {
      return b;
    }
    int pub() {
      return a;
    }
};
int main()
{
  publicInh object1;
  cout << "Private = " << object1.pri() << endl;
  cout << "Protected = " << object1.pro() << endl;
  cout << "Public = " << object1.a<< endl;
  protectedInh object2;
  cout << "Private cannot be accessed." << endl;
  cout << "Protected = " << object2.pro() << endl;
  cout << "Public = " << object2.pub() << endl;
  privateInh object3;
  cout << "Private cannot be accessed." << endl;
  cout << "Protected = " << object3.pro() << endl;
  cout << "Public = " << object3.pub() << endl;
  return 0;
}
