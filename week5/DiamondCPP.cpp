#include <iostream>
using namespace std;
 
class A {
public:
  A()  { cout << "Class A constructor called" << endl; }
};
 
class B: public A {
public:
  B()  { cout << "Class B constructor called" << endl; }
};
 
class C: public A {
public:
  C()  { cout << "Class C constructor called" << endl; }
};
 
class D: public B, public C {
public:
  D()  { cout << "Class D constructor called" << endl; }
};
 
int main() {
    D d;
}